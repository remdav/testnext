import {
  CanActivate,
  ExecutionContext,
  HttpException,
  Injectable,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { RoleUser, TokenData } from 'src/type/app/user';
import { ErrorCodeHttp, ErrorMessage } from 'src/type/message/error';
import { ManageJwt } from '.';

@Injectable()
export class AuthGuard implements CanActivate {
  manageJwt: ManageJwt;

  constructor(private reflector?: Reflector) {
    this.manageJwt = new ManageJwt();
  }

  canActivate(context: ExecutionContext) {
    const { rawHeaders, url } = context.switchToHttp().getRequest();

    if (url === '/user/refresh') {
      return this.manageJwt.verifTokenBool(rawHeaders, true);
    }

    const token = this.manageJwt.getToken(rawHeaders);

    if (token === process.env.APP_ACCESS) {
      return true;
    } else {
      const role = this.reflector.get<RoleUser>('role', context.getHandler());
      const data: TokenData = this.manageJwt.decode(token, false);
      const isAuthorized = data.roles.some((roleUser) => role === roleUser);

      if (isAuthorized) {
        return true;
      }

      throw new HttpException(ErrorMessage.FORBIDDEN, ErrorCodeHttp.FORBIDDEN);
    }
  }
}

export default AuthGuard;

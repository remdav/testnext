import * as bcrypt from 'bcrypt';

class ManageHash {
  salt: number;

  constructor() {
    this.salt = parseInt(process.env.SALT_BCRYPT);
  }

  async hash(password: string): Promise<string> {
    return await bcrypt.hash(password, this.salt);
  }

  async compare(password: string, hash: string): Promise<boolean> {
    return await bcrypt.compare(password, hash);
  }
}

export default ManageHash;

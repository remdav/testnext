import { HttpException } from '@nestjs/common';
import { sign, verify } from 'jsonwebtoken';
import { ErrorCodeHttp, ErrorMessage } from 'src/type/message/error';

class ManageJwt {
  private password: string;
  private passwordRt: string;

  constructor() {
    this.password = process.env.PASSWD_JWT;
    this.passwordRt = process.env.PASSWD_RT_JWT;
  }

  private generateToken(
    data: { [key: string]: any },
    expiresIn: string,
    isRefresh = false,
  ) {
    return sign(data, isRefresh ? this.passwordRt : this.password, {
      algorithm: 'HS512',
      expiresIn,
    });
  }

  getToken(req: string[]): string {
    const bearerToken = req.find((header) => header.startsWith('Bearer'));
    const token = bearerToken.split(' ')[1];

    if (token) {
      return token;
    }

    throw new HttpException(ErrorMessage.FORBIDDEN, ErrorCodeHttp.FORBIDDEN);
  }

  decode(token: string, isRefresh: boolean): any {
    try {
      return verify(token, isRefresh ? this.passwordRt : this.password);
    } catch {
      throw new HttpException(ErrorMessage.FORBIDDEN, ErrorCodeHttp.FORBIDDEN);
    }
  }

  verifTokenBool(req: string[], isRefresh: boolean): boolean {
    const token = this.getToken(req);

    if (token) {
      this.decode(token, isRefresh);

      return true;
    }

    throw new HttpException(ErrorMessage.FORBIDDEN, ErrorCodeHttp.FORBIDDEN);
  }

  signAccessToken(data: { [key: string]: any }): string {
    return this.generateToken(data, '15m');
  }

  signRefreshToken(data: { [key: string]: any }) {
    return this.generateToken(data, '15d', true);
  }
}

export default ManageJwt;

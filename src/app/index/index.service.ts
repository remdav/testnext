import { Injectable } from '@nestjs/common';

@Injectable()
class IndexServices {
  homePage(): string {
    return 'Home Page';
  }
}

export default IndexServices;

import { Controller, Get } from '@nestjs/common';
import IndexServices from './index.service';

@Controller()
class IndexController {
  constructor(private readonly indexServices: IndexServices) {}

  @Get()
  index(): string {
    return this.indexServices.homePage();
  }
}

export default IndexController;

import { Exclude, Expose } from 'class-transformer';
import { RoleUser } from 'src/type/app/user';
import { GroupUser } from 'src/type/serialize/User';
import {
  Column,
  CreateDateColumn,
  Entity,
  Index,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
class User {
  @PrimaryGeneratedColumn('uuid')
  @Expose({ groups: [GroupUser.INFO_USER, GroupUser.INFO_USER_MORE] })
  uuid: string;

  @Index()
  @Column({ type: 'varchar', length: 50, unique: true })
  @Expose({ groups: [GroupUser.INFO_USER, GroupUser.INFO_USER_MORE] })
  username: string;

  @Column({ type: 'varchar', length: 80 })
  @Exclude()
  password: string;

  @Column('varchar', { array: true, default: [RoleUser.USER] })
  @Expose({ groups: [GroupUser.INFO_USER, GroupUser.INFO_USER_MORE] })
  roles: RoleUser[];

  @CreateDateColumn()
  @Expose({ groups: [GroupUser.INFO_USER_MORE] })
  createAt: Date;

  @UpdateDateColumn()
  @Expose({ groups: [GroupUser.INFO_USER_MORE] })
  updateAt: Date;
}

export default User;

import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Get,
  HttpException,
  Post,
  Req,
  SerializeOptions,
  UseInterceptors,
} from '@nestjs/common';
import { ManageHash, ManageJwt } from 'src/helper/security';
import { RoleUser, TokenData } from 'src/type/app/user';
import { ErrorCodeHttp, ErrorMessage } from 'src/type/message/error';
import { GroupUser } from 'src/type/serialize/User';
import { Roles } from './decorator';
import { CreateUserDto } from './dto/create.dto';
import { LoginDto } from './dto/Login.dto';
import User from './user.entity';
import UserService from './user.service';

interface LoginResponse {
  accessToken: string;
  refreshToken: string;
}

@Controller('/user')
@UseInterceptors(ClassSerializerInterceptor)
class UserController {
  manageHash: ManageHash;
  manageJwt: ManageJwt;

  constructor(private readonly userService: UserService) {
    this.manageHash = new ManageHash();
    this.manageJwt = new ManageJwt();
  }

  private prepareResponseToken(userInfo: TokenData | User): LoginResponse {
    const data = {
      uuid: userInfo.uuid,
      username: userInfo.username,
      roles: userInfo.roles,
    };

    return {
      accessToken: this.manageJwt.signAccessToken(data),
      refreshToken: this.manageJwt.signRefreshToken(data),
    };
  }

  @Get()
  @SerializeOptions({
    groups: [GroupUser.INFO_USER],
  })
  @Roles(RoleUser.ADMIN)
  getDataUser(@Req() req): Promise<User> {
    const token: string = this.manageJwt.getToken(req.rawHeaders);
    const { uuid }: TokenData = this.manageJwt.decode(token, false);

    return this.userService.finnOneById(uuid);
  }

  @Post()
  @SerializeOptions({
    groups: [GroupUser.INFO_USER],
  })
  async create(@Body() data: CreateUserDto) {
    const password = await this.manageHash.hash(data.password);

    return this.userService.create({ ...data, password });
  }

  @Post('/login')
  async login(@Body() data: LoginDto): Promise<LoginResponse> {
    const infoUser = await this.userService.findOneBy({
      username: data.username,
    });

    const isGoodPassword = await this.manageHash.compare(
      data.password,
      infoUser.password,
    );

    if (isGoodPassword) {
      return this.prepareResponseToken(infoUser);
    }

    throw new HttpException(ErrorMessage.BAD_LOGIN, ErrorCodeHttp.BAD_REQUEST);
  }

  @Post('/refresh')
  async refreshToken(@Req() req): Promise<LoginResponse> {
    const token = this.manageJwt.getToken(req.rawHeaders);
    const userInfo: TokenData = this.manageJwt.decode(token, true);

    return this.prepareResponseToken(userInfo);
  }
}

export default UserController;

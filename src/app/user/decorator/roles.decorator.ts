import { SetMetadata } from '@nestjs/common';
import { RoleUser } from 'src/type/app/user';

const Roles = (role: RoleUser) => SetMetadata('role', role);

export default Roles;

import { HttpException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ErrorCodeHttp, ErrorMessage } from 'src/type/message/error';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create.dto';
import User from './user.entity';

@Injectable()
class UserService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
  ) {}

  finnOneById(uuid: string): Promise<User> {
    return this.userRepository.findOne(uuid);
  }

  findOneBy(obj: { [key: string]: any }): Promise<User> {
    return this.userRepository.findOne(obj);
  }

  async create(data: CreateUserDto): Promise<User> {
    let newUser;

    try {
      newUser = await this.userRepository.save(data);
    } catch (error) {
      if (error.code === '23505') {
        throw new HttpException(
          ErrorMessage.USER_ALREADY_EXIST,
          ErrorCodeHttp.BAD_REQUEST,
        );
      }

      throw new HttpException(
        ErrorMessage.UNKNOW,
        ErrorCodeHttp.INTERNAL_SERVER_ERROR,
      );
    }

    return this.finnOneById(newUser.uuid);
  }
}

export default UserService;

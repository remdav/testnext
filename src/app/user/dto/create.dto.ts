import { MaxLength, MinLength } from 'class-validator';
import { ErrorMessage } from 'src/type/message/error';

export class CreateUserDto {
  @MinLength(2, { message: ErrorMessage.TOO_SHORT })
  @MaxLength(50, { message: ErrorMessage.TOO_LONG })
  username: string;

  @MinLength(5, { message: ErrorMessage.TOO_SHORT })
  @MaxLength(100, { message: ErrorMessage.TOO_LONG })
  password: string;
}

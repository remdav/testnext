import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { IndexController, IndexServices } from './index/';
import { UserModule } from './user';

export const controllers = [IndexController];

export const providers = [IndexServices];

export const imports = [
  ConfigModule.forRoot({
    envFilePath: `.env.${process.env.NODE_ENV}`,
  }),
  TypeOrmModule.forRootAsync({
    imports: [ConfigModule, UserModule],
    useFactory: (configService: ConfigService) => ({
      type: configService.get<'mysql' | 'mariadb' | 'postgres'>('TYPE_DB'),
      host: configService.get<string>('HOST_DB'),
      port: parseInt(configService.get<string>('PORT_DB')),
      username: configService.get<string>('USER_DB'),
      password: configService.get<string>('PASSWD_DB'),
      database: configService.get<string>('DATABASE_DB'),
      autoLoadEntities: true,
      synchronize: Boolean(configService.get<string>('SYNCHRONIZE_DB')),
    }),
    inject: [ConfigService],
  }),
];

import { Module } from '@nestjs/common';
import { controllers, imports, providers } from './app';

@Module({
  controllers,
  providers,
  imports,
})
export class AppModule {}

export enum RoleUser {
  ADMIN = 'ADMIN',
  USER = 'USER',
  APP = 'APP',
}

export interface TokenData {
  uuid: string;
  username: string;
  roles: string[];
  iat: number;
  exp: number;
}
